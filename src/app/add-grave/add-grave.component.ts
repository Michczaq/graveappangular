import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Grave } from '../../model/grave';
import { GravesService } from '../services/graves.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-add-grave',
  templateUrl: './add-grave.component.html',
  styleUrls: ['./add-grave.component.css']
})
export class AddGraveComponent implements OnInit {

  @ViewChild('fileInput') fileInput: ElementRef;
  dateOfBirthPicker2 = '';
  dateOfDeathPicker2 = '';

  nameOfGrave = '';
  dateOfBirthPicker: Date;
  dateOfDeathPicker: Date;
  cemetaryNameInput: string;
  cityNameInput: string;
  b64Input: string;

  constructor(private gravesService: GravesService) { }

  ngOnInit() {
  }


  onFileChange(event) {
    // tslint:disable-next-line:prefer-const
    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      // tslint:disable-next-line:prefer-const
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.b64Input = reader.result;
      };
    }
  }

  clearFile() {
    this.fileInput.nativeElement.value = '';
  }

  add() {
    if (this.dateOfBirthPicker == null ) { this.dateOfBirthPicker2 = ' '; } else {
      this.dateOfBirthPicker2 = this.dateOfBirthPicker.toLocaleString();
    }

    if (this.dateOfDeathPicker == null ) { this.dateOfDeathPicker2 = ' '; } else {
      this.dateOfDeathPicker2 = this.dateOfDeathPicker.toLocaleString();
    }

    const grave: Grave = {
      name: this.nameOfGrave,
      dateOfBirth: this.dateOfBirthPicker2,
      dateOfDeath: this.dateOfDeathPicker2,
      cemetaryName: this.cemetaryNameInput,
      city: this.cityNameInput,
      imageUrl: this.b64Input,

    };
    this.gravesService.add(grave);
    this.clearFields();
  }



  clearFields() {
    this.nameOfGrave = '';
    this.dateOfBirthPicker = null;
    this.dateOfDeathPicker = null;
    this.cemetaryNameInput = '';
    this.cityNameInput = '';
    this.fileInput.nativeElement.value = '';
  }
}
