import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Grave } from '../../model/grave';
import { GravesService } from '../services/graves.service';
import { FilterPipe } from '../../shared/filter.pipe';

@Component({
  selector: 'app-view-grave',
  templateUrl: './view-grave.component.html',
  styleUrls: ['./view-grave.component.css']
})
export class ViewGraveComponent implements OnInit {

  term: string;
  graveList: Array<Grave> = [];
  imageUrl: string;

  constructor(private gravesService: GravesService) {
    this.gravesService.getGravesListObs().subscribe((graves: Array<Grave>) => {
      this.graveList = graves;
    });
  }

  ngOnInit() {
  }

  remove(grave: Grave) {
    this.gravesService.remove(grave);
  }


  getColor(): string {
    return this.graveList.length >= 100 ? 'red' : 'green';
  }

}
