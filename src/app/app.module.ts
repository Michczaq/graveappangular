import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AddGraveComponent } from './add-grave/add-grave.component';
import { HttpClientModule } from '@angular/common/http';
import { GravesService } from './services/graves.service';
import { HttpService } from './services/http.service';
import { ViewGraveComponent } from './view-grave/view-grave.component';
import { FilterPipe } from '../shared/filter.pipe';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { SliderComponent } from './slider/slider.component';


@NgModule({
  declarations: [
    AppComponent,
    AddGraveComponent,
    ViewGraveComponent,
    FilterPipe,
    SliderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgbModule.forRoot()
  ],
  providers: [GravesService, HttpService],
  bootstrap: [AppComponent]
})
export class AppModule { }
