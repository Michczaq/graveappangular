import { Component } from '@angular/core';
import { GravesService } from './services/graves.service';
import { providerDef } from '@angular/core/src/view/provider';
import { FilterPipe } from '../shared/filter.pipe';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private gravesService: GravesService) {

  }

  save() {
    this.gravesService.saveGravesInDb();
  }

}
