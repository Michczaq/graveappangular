import { Injectable } from '@angular/core';
import { HttpParams, HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Grave } from '../../model/grave';

@Injectable()
export class HttpService {

  readonly URL_DB = 'https://api.mlab.com/api/1/databases/grave_db/collections/graves';
  readonly param = new HttpParams().set('apiKey', 'AYwYj_hQ8ODjB3z-Q-z-FeNi5TkJ9K69');

  constructor(private http: HttpClient) {
    this.getGraves();
  }

  getGraves(): Observable<Array<Grave>> {
    return this.http.get<Array<Grave>>(this.URL_DB, { params: this.param});
  }


  saveGraves(list: Array<Grave>) {
    this.http.put(this.URL_DB, list, { params: this.param })
    .subscribe(data => {
    console.log(data);
    });
  }

}
