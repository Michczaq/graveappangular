import { Injectable } from '@angular/core';
import { Grave } from '../../model/grave';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { HttpService } from './http.service';

@Injectable()
export class GravesService {


  private graveListObs = new BehaviorSubject<Array<Grave>>([]);

  constructor(private httpService: HttpService) {
    this.httpService.getGraves().subscribe(list => {
      this.graveListObs.next(list);
    });
  }

add(grave: Grave) {
  const list = this.graveListObs.getValue();
  list.push(grave);
  this.graveListObs.next(list);
}

remove(grave: Grave) {
  const list = this.graveListObs.getValue().filter(e => e !== grave);
  this.graveListObs.next(list);
}

// edit(grave: Grave) {
//   const list = this.graveListObs.getValue();
//   list.push(grave);
//   this.graveListObs.next(list);
// }




getGravesListObs(): Observable<Array<Grave>> {
  return this.graveListObs.asObservable();
}

saveGravesInDb() {
  this.httpService.saveGraves(this.graveListObs.getValue());
}


}

