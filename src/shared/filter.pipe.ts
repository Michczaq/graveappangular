import { Pipe, PipeTransform } from '@angular/core';
import { Grave } from '../model/grave';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(graves: Array<Grave>, term: ''): Array<Grave> {
    // if undefined
    if (term === undefined) { return graves; }
    return graves.filter(function(grave) {
      return grave.name.toLowerCase().includes(term.toLowerCase())
      || grave.cemetaryName.toLowerCase().includes(term.toLowerCase())
      || grave.city.toLowerCase().includes(term.toLowerCase())
      || grave.dateOfBirth.toLowerCase().includes(term.toLowerCase())
      || grave.dateOfDeath.toLowerCase().includes(term.toLowerCase());
    });
  }

}
