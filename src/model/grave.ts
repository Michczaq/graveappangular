export interface Grave {
  _id?: { $oid: string};
  name: string;
  dateOfBirth: string;
  dateOfDeath?: string;
  cemetaryName?: string;
  city?: string;
  imageUrl?: string;
}
